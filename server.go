package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

var server = &http.Server{
	WriteTimeout: time.Second * 5,
	ReadTimeout:  time.Second * 5,
	IdleTimeout:  time.Second * 30,
}

func start() {
	server.Handler = router

	go func() {
		log.Printf("tarchur v%s listening on %s", version, server.Addr)
		if err := server.ListenAndServe(); err != nil {
			log.Fatal(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	server.Shutdown(ctx)

	log.Printf("shutting down...")
	os.Exit(0)
}

func init() {
	server.Addr = os.Getenv("SERVER_ADDR")
}
