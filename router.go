package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
)

var router *mux.Router

func init() {
	router = mux.NewRouter()
	router.Methods("GET").
		Path("/transmissions/{id}").
		HandlerFunc(getTransmission)
	router.Methods("POST").
		Path("/transmissions").
		Headers("Content-Type", "application/json").
		HandlerFunc(createTransmission)
	router.Methods("GET").
		PathPrefix("/").
		Handler(http.StripPrefix("/", handlers.CORS()(http.FileServer(http.Dir("www")))))
}

func createTransmission(w http.ResponseWriter, r *http.Request) {
	t := &transmission{}

	err := json.NewDecoder(r.Body).Decode(t)
	if err != nil {
		log.Print(err)
		http.Error(w, "invalid JSON", http.StatusBadRequest)
		return
	}

	err = t.test()
	if err != nil {
		log.Print(err)
		http.Error(w, "cannot contact server", http.StatusBadGateway)
		return
	}

	t.ID, err = uuid.NewV4()
	if err != nil {
		log.Print(err)
		http.Error(w, "failed to generate a UUID", http.StatusInternalServerError)
		return
	}

	err = t.save()
	if err != nil {
		log.Print(err)
		http.Error(w, "failed to save transmission config", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Location", fmt.Sprintf("/transmissions/%s", t.ID))
	w.WriteHeader(http.StatusCreated)
}

func getTransmission(w http.ResponseWriter, r *http.Request) {
	t := &transmission{}
	id, found := mux.Vars(r)["id"]

	if !found {
		log.Fatal("Expected an ID var; routes are set up incorrectly")
	}

	log.Printf("Getting transmission %s", id)

	found, err := t.load(id)
	if !found {
		http.NotFound(w, r)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	err = json.NewEncoder(w).Encode(t)
	if err != nil {
		log.Print(err)
		http.Error(w, "failed decoding transmission", http.StatusInternalServerError)
	}
}
