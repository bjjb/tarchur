package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"time"

	uuid "github.com/satori/go.uuid"
)

type transmission struct {
	ID        uuid.UUID `json:"id"`
	Host      string    `json:"host"`
	Username  string    `json:"username"`
	Password  string    `json:"password"`
	SessionID string    `json:"session_id,omitempty"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

var client *http.Client

func (t *transmission) save() error {
	tx, err := database.Begin()
	if err != nil {
		return fmt.Errorf("failed to start transaction")
	}

	stmt, err := tx.Prepare(`
		INSERT INTO transmissions(id, host, username, password, created_at, updated_at)
								VALUES(?, ?, ?, ?, ?, ?);
	`)
	if err != nil {
		return fmt.Errorf("failed to prepare statement")
	}
	defer stmt.Close()

	_, err = stmt.Exec(t.ID, t.Host, t.Username, t.Password, time.Now(), time.Now())
	if err != nil {
		return fmt.Errorf("failed to execute statement")
	}

	tx.Commit()
	return nil
}

func (t *transmission) load(id string) (bool, error) {
	row := database.QueryRow(`
		SELECT id, host, username, password, created_at, updated_at
			FROM transmissions
		 WHERE id=?;`, id)
	err := row.Scan(&t.ID, &t.Host, &t.Username, &t.Password, &t.CreatedAt, &t.UpdatedAt)
	switch {
	case err == sql.ErrNoRows:
		return false, nil
	case err != nil:
		log.Print(err)
		return false, err
	default:
		return true, nil
	}
}

func (t *transmission) test() error {
	if t.Host == "" {
		return fmt.Errorf("Missing host")
	}
	if t.Username == "" {
		return fmt.Errorf("Missing username")
	}
	if t.Password == "" {
		return fmt.Errorf("Missing password")
	}
	sessionID, err := get(t.Host, t.Username, t.Password, "")
	if err != nil {
		return err
	}
	t.SessionID = sessionID
	return nil
}

func get(host, username, password, session string) (string, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/transmission/rpc", host), nil)
	if err != nil {
		log.Print(err)
		return "", fmt.Errorf("Failed to make request")
	}
	if username != "" && password != "" {
		req.SetBasicAuth(username, password)
	}
	if session != "" {
		req.Header.Set("X-Transmission-Session-Id", session)
	}
	res, err := client.Do(req)
	if err != nil {
		return "", err
	}
	return res.Header.Get("X-Transmission-Session-Id"), nil
}

func init() {
	client = &http.Client{}
}
