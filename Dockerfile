FROM scratch
COPY www ./
COPY tarchur ./
ENV WEB_ROOT=www
ENV BIND_ADDR=:8800
EXPOSE 8800
CMD ["/tarchur"]
# vi: tw=0
