package main

import (
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/gorilla/sessions"
)

const letters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const sessionName = "tarchur"

var sessionStore sessions.Store

func init() {
	rand.Seed(time.Now().UnixNano())

	sessionSecret := os.Getenv("SESSION_SECRET")
	if sessionSecret == "" {
		log.Print("Warning: no SESSION_SECRET; sessions will not persist")
		sessionSecret = generateSecret(40)
	}
	sessionStore = sessions.NewCookieStore([]byte(sessionSecret))
}

func generateSecret(length int) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
