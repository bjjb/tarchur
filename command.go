package main

import (
	"github.com/spf13/cobra"
)

var command = &cobra.Command{
	Use:   "tarchur",
	Short: "manage transmission clients remotely",
	Long:  `Tarchur controls a running transmission client on behalf of a user`,
	Run: func(cmd *cobra.Command, args []string) {
		start()
	},
}
