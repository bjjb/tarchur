package main

import (
	"database/sql"
	"log"
	"net/url"
	"os"
	"strings"

	_ "github.com/mattn/go-sqlite3"
)

const schema = `
CREATE TABLE IF NOT EXISTS transmissions (
	id 					VARCHAR(36) 	NOT NULL UNIQUE PRIMARY KEY,
	host 				VARCHAR(255) 	NOT NULL,
	username 		VARCHAR(255) 	NOT NULL,
	password 		VARCHAR(255) 	NOT NULL,
	session_id	VARCHAR(64),
	created_at	DATETIME			NOT NULL,
	updated_at 	DATETIME			NOT NULL
);`

var database *sql.DB

func init() {
	dsn := os.Getenv("DATABASE_URL")
	if dsn == "" {
		dsn = "sqlite3::memory:"
	}
	uri, err := url.Parse(dsn)
	if err != nil {
		log.Print(err)
		log.Fatalf("Failed to parse database url: %s", dsn)
	}
	driver := strings.TrimSuffix(uri.Scheme, ":")
	database, err = sql.Open(driver, dsn)
	if err != nil {
		log.Print(err)
		log.Fatalf("Failed to open database %s (%s)", dsn, driver)
	}
	_, err = database.Exec(schema)
	if err != nil {
		log.Print(err)
		log.Fatalf("Failed to update database")
	}
}
