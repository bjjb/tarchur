.PHONY: test clean

default: tarchur

tarchur: test *.go
	@ go build

fmt:
	@ go fmt

test: fmt
	@ go test
	@ go vet
	@ golint

clean:
	@ go clean
